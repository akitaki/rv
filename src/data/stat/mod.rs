mod bernoulli;
mod categorical;
mod gaussian;
mod mvg;
mod poisson;

pub use bernoulli::*;
pub use categorical::*;
pub use gaussian::*;
pub use mvg::*;
pub use poisson::*;
